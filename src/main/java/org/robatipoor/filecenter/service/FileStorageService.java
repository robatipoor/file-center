package org.robatipoor.filecenter.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

import org.robatipoor.filecenter.exception.FileStorageException;
import org.robatipoor.filecenter.exception.LoadFileStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * FileStorageService
 */
@Service
public class FileStorageService {

    private static final Logger log = LogManager.getLogger(FileStorageService.class);
    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService() {
        this.fileStorageLocation = Paths.get("").toAbsolutePath().normalize();
        log.info("File Storage Location {}", this.fileStorageLocation.toString());
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.");
        }
    }

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            log.info("Store File");
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file ");
        }
    }

    public Resource loadFile(String fileName) {

        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new LoadFileStorageException("File not found ");
            }
        } catch (MalformedURLException ex) {
            throw new LoadFileStorageException("File not found ");
        }
    }

    public List<Resource> listFile() throws IOException {

        return Files.list(this.fileStorageLocation).filter((path) -> {
            return path.toFile().isFile();
        }).map((path) -> {
            try {
                log.debug(path.toString());
                return new UrlResource(path.toUri());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                throw new RuntimeException();
            }
        }).collect(Collectors.toList());
    }
}