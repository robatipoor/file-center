package org.robatipoor.filecenter.exception;

/**
 * FileStorageException
 */
public class FileStorageException extends RuntimeException {

    private static final long serialVersionUID = 5209126564795272015L;

    public FileStorageException(String msg) {
        super(msg);
    }
}